# Local_Files_Backup_Scripts

Batch script for sync local files in windows.

**Usage**:

(1) `all2one` can sync ALL SELECTED dirs to ONE specific dir. (Currently, It will not remove deleted file in designated dir

1. edit `backup_org.bat`, copy it to source dir, then change it's name to `backup.bat`
2. edit `list_all.bat`
3. execute `run.bat` to run the script


(2) `one2all` can sync ALL FILES & DIRS in ONE specific dir to ALL SELECTED dirs.

1. edit `run.bat`
2. execute `run.bat` to run the script
