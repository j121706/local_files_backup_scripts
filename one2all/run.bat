:: Variables:
:: set your dir after from & to 
:: Except set, there cannot be any space in your dir, also between equal sign

:: Source dir
set from=

:: set destination dirs, new a line in SET & rmdir & ROBOCOPY to add destination dirs
set to=
:: set to_2=
:: set to_3=
:: set ...

rmdir /S /Q %to%
:: rmdir /S /Q %to_2%
:: rmdir /S /Q %to_3%
:: rmdir /S /Q ...

:: You can add /XD to exclude specific dirs
:: You can add /XF to exclude specific files
ROBOCOPY %from% %to% /R:0 /S /XO 
:: ROBOCOPY %from% %to_2% /R:0 /S /XO 
:: ROBOCOPY %from% %to_3% /R:0 /S /XO 
:: ROBOCOPY %from% ... 
