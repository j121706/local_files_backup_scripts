:: Variables:
:: set your dir after from & to 
:: Except set, there cannot be any space in your dir, also between equal sign

:: if you want to set current dir as the source dir, you can use from=%~dp0
set from=
set to=

:: ---- START SCRIPT ----

:: You can add /XD to exclude specific dirs
:: You can add /XF to exclude specific files
ROBOCOPY %from% %to% /R:0 /S /XO

:: ---- END SCRIPT ----
