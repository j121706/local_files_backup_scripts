:: Readme
:: (1) This script will new a txt then delete it after operation. (tmp.txt)
:: (2) set your scan dir, it must be the most top level.
:: (3) set your script dir

set scan_dir=
set script_dir=

:: ---- START SCRIPT ----

break>tmp.txt

cd %scan_dir% && for /r %%x in (*.bat) do echo call %%x>> %script_dir%\tmp.txt

cd %script_dir%

type tmp.txt | findstr /m "backup.bat" > out.bat

del tmp.txt

out.bat

:: ---- END SCRIPT ----
